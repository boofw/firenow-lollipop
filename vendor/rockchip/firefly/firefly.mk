CUR_PATH := vendor/rockchip/firefly/

-include $(CUR_PATH)/catlog/catlog.mk
-include $(CUR_PATH)/hdmiin/hdmiin.mk
include $(CUR_PATH)/apps/apps.mk
include $(CUR_PATH)/usb_mode_switch/usb_mode_switch.mk
include $(CUR_PATH)/firefly_sdkapi_demo/firefly_sdkapi_demo.mk
