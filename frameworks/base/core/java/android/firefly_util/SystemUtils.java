package android.firefly_util;

import java.io.File;
import android.content.Context;
import android.content.Intent;

public class SystemUtils {

	private static final String ACTION_FIREFLY_SCREENSHOT = "action_firefly_screenshot";
	private static final String EXTRA_KEY_PATH  = "extra_key_path";
	private static final String EXTRA_KEY_NAME  = "extra_key_name";
	public static void takeScreenshot(Context context,String path,String name){
		Intent intent = new Intent(ACTION_FIREFLY_SCREENSHOT);  
		intent.putExtra(EXTRA_KEY_PATH, path); 
		intent.putExtra(EXTRA_KEY_NAME, name); 
		context.sendBroadcast(intent);
	}


	/**
	 * 
	 * @param rotation    　Surface.ROTATION_0
	 * 								    Surface.ROTATION_90
	 * 								    Surface.ROTATION_180
	 * 								    Surface.ROTATION_270
	 */
	public static void setRotation (Context context ,int  rotation){

	}


	private static  final String backligh_file_path = "/sys/class/backlight/rk28_bl/brightness";
	private static  final String CONFIG_LAST_BACKLIGHT = "config_last_backlight";
	public static  void backlight_close(Context context)
	{
		File file = new File(backligh_file_path);
		String current_backlight = FileUtils.readFromFile(file);
		if(!"0".equals(current_backlight))
		{	
			SharedPreferencesUtils.setParam(context, CONFIG_LAST_BACKLIGHT, current_backlight);
			FileUtils.write2File(file,"0");
		}
	}


	public static void bakclight_open(Context context)
	{
		File file = new File(backligh_file_path);
		String current_backlight = FileUtils.readFromFile(file);
		if("0".equals(current_backlight))
		{	
			String last_backlight = (String) SharedPreferencesUtils.getParam(context, CONFIG_LAST_BACKLIGHT, "0");
			if(!"0".equals(last_backlight))
			{
				FileUtils.write2File(file,last_backlight);
			}
			SharedPreferencesUtils.deleteParam(context, CONFIG_LAST_BACKLIGHT);
		}
	}


}
